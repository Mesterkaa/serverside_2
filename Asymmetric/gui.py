import PySimpleGUI as sg
import sys
sys.path.append("C:\Repos\Cryptography\Asymmetric")
import Asymmetric
from importlib import reload # reload 
reload(Asymmetric)
from enum import Enum



def main():
    class Fields(Enum):
        GenerateKeys_Btn = "Generate Keys"
        PrivateKey = "PRIVATE_KEY"
        PublicKey = "PUBLIC_KEY"
        Encode_Btn = "Encode"
        Decode_Btn = "Decode"
        Input = "INPUT"
        Output = "OUTPUT"

    privatekey = None
    publickey = None


    layout = [
        [
            sg.Column([
                [sg.Button(Fields.GenerateKeys_Btn.value)],
                [sg.Multiline(size=(40, 10), key=Fields.PrivateKey.value)],
                [sg.Multiline(size=(40, 10), key=Fields.PublicKey.value)]
            ]),
            sg.VSeperator(),
            sg.Column([
                [sg.Text("INPUT"), sg.Button(Fields.Encode_Btn.value), sg.Button(Fields.Decode_Btn.value)],
                [sg.Multiline(size=(40, 20), key=Fields.Input.value)]
            ]),
            sg.VSeperator(),
            sg.Column([
                [sg.Text("OUTPUT")],
                [sg.Multiline(size=(40, 20), key=Fields.Output.value)]
            ])
        ]
    ]
    window = sg.Window(title="RSA Encryption", layout=layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == Fields.GenerateKeys_Btn.value:
            print("Generating Keys..")
            privatekey, publickey = Asymmetric.generate_keys()
            window[Fields.PrivateKey.value].update(privatekey.exportKey().decode())
            window[Fields.PublicKey.value].update(publickey.exportKey().decode())
        elif event == Fields.Encode_Btn.value:
            print("Encoding Message")
            window[Fields.Output.value].update(Asymmetric.encrypt_message(values[Fields.Input.value][0:190], publickey))
        elif event == Fields.Decode_Btn.value:
            print("Decoding Message")
            window[Fields.Output.value].update(Asymmetric.decrypt_message(values[Fields.Input.value], privatekey))

if __name__ == "__main__":
    main()