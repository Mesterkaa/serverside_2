from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64


def generate_keys():
   # key length must be a multiple of 256 and >= 1024
   modulus_length = 256*8
   privatekey = RSA.generate(modulus_length, Random.new().read)
   publickey = privatekey.publickey()
   print(privatekey.exportKey())
   print(publickey.exportKey())
   return privatekey, publickey

def encrypt_message(a_message , publickey):
   encryptor = PKCS1_OAEP.new(publickey)
   encrypted_msg = encryptor.encrypt(a_message.encode())
   encoded_encrypted_msg = base64.b64encode(encrypted_msg).decode()
   return encoded_encrypted_msg

def decrypt_message(encoded_encrypted_msg, privatekey):
   decryptor = PKCS1_OAEP.new(privatekey)
   decoded_encrypted_msg = base64.b64decode(encoded_encrypted_msg)
   decoded_decrypted_msg = decryptor.decrypt(decoded_encrypted_msg).decode()
   return decoded_decrypted_msg
