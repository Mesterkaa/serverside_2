from itertools import cycle
import base64


def cryptString(data, key, encode = False, decode = False):

    # I need to encode/decode the data before doing anything, to make sure any control characters don't mess anythin up. 
    if decode:
        data = base64.b64decode(data).decode()
    #cryptList = zip(data, cycle(key))
    #xored = ""
    #for (x, y) in cryptList:
    #    xored += chr(ord(x) ^ ord(y))

    # This is the way i XOR every letter of the data, with the kodeword.
    # It uses Join and it's one line, but it does the same as the above commeted out code.

    # First it creates a list of letter pairs. The first letter of the data and the first letter of the key, and so on.
    # zip() create the object array.
    # If the key is sortere than the data, cycle(), goes back to the beginning of the key, and starts again.
    # ord() gets the binary value of the letter
    # ^ is the bitwise XOR operate
    # chr(), when takes the new binary value and converts it back to a ASCII character.
    xored  = ''.join(chr(ord(x) ^ ord(y)) for (x,y) in zip(data, cycle(key)))

    if encode:
        return base64.b64encode(xored.encode()).decode()
    return xored
