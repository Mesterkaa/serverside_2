import sys
sys.path.append("C:\Repos\Cryptography\XOR")
import PySimpleGUI as sg
import XOR
from importlib import reload # reload 
reload(XOR)

def main():
    layout = [
        [sg.Button("ENCODE")],
        [sg.Text("Password: ", size=(15, 1)), sg.Input(key='PASS',size=(30,1),default_text='Password')],
        [sg.Button("DECODE")],
        [sg.Multiline(size=(60, 5), key='TEXT', default_text='Meget Hemmelig Besked')]
    ]
    window = sg.Window(title="XOR Encryption", layout=layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == "ENCODE":
            ## Når man trykker ENCODE bruges Caesar-Code funktionen til at kryptere teksten. PASS bliver brugt som kodeord. Tekst feltet bliver så updateret med den krypterede tekst.
            window['TEXT'].update(XOR.cryptString(values['TEXT'], values['PASS'], encode = True))
        elif event == "DECODE":
            ## Når man trykker DECODE bruges Caesar-Code funktionen til at dekryptere teksten. PASS bliver brugt som kodeord. Tekst feltet bliver så updateret med den dekrypterede tekst..
            window['TEXT'].update(XOR.cryptString(values['TEXT'], values['PASS'], decode = True))
        
if __name__ == "__main__":
    main()