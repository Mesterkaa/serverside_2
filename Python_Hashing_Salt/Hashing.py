import hashlib

def Hash(text, salt, type):
    # The same is done for every type. It first encodes the text, to make sure no illegal character is used.
    # Together with the salt, the text is hashed. hexdigest() converts the hash back to a string.
    if type == "SHA256":
        return hashlib.sha256((salt + text).encode()).hexdigest()
    elif type == "SHA512":
        return hashlib.sha512((salt + text).encode()).hexdigest()
    elif type == "SHA1":
        return hashlib.sha1((salt + text).encode()).hexdigest()
    elif type == "SHA3_512":
        return hashlib.sha3_512((salt + text).encode()).hexdigest()
    elif type == "MD5":
        return hashlib.md5((salt + text).encode()).hexdigest()
