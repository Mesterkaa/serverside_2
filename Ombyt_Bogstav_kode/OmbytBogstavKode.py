def TranslateText(text):
    # It takes the two first letters and switches them. The two next letter is also switched and so on.
    # If the string length is odd, the last letter is added to the end.
    newText = ""
    for x in range(0, len(text) - 1, 2):
        newText += text[x + 1].lower()
        newText += text[x].lower()
    if len(text) % 2 == 1:
        newText += text[len(text) - 1].lower()
    return newText
