import sys
sys.path.append("C:\Repos\Cryptography\Ombyt_Bogstav_kode")
import PySimpleGUI as sg
import OmbytBogstavKode as Code
from importlib import reload # reload 
reload(Code)

def main():
    layout = [
        [sg.Button("TRANSLATE")],
        [sg.Multiline(size=(60, 5), key='TEXT', default_text='hello world')]
    ]
    window = sg.Window(title="OmbytBogstavKode", layout=layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == "TRANSLATE":
            ## Når man trykker TRANSLATE, bliver teksten kørt i gennem krypterings algoriment. Den er simple og kræver ikke et kode ord.
            window['TEXT'].update(Code.TranslateText(values['TEXT']))
        
if __name__ == "__main__":
    main()