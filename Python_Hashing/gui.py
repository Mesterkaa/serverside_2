import sys
sys.path.append("C:\Repos\Cryptography\Python_Hashing")
import PySimpleGUI as sg
import Hashing as Hash
from importlib import reload # reload 
reload(Hash)

def main():
    layout = [
        [sg.Button("HASH")],
        [sg.Text("Text to hash: ", size=(15, 1)), sg.Input(key='TEXT',size=(30,1),default_text='Hello')],
        [sg.Text("SHA256:"), sg.Text("--", key='SHA256')],
        [sg.Text("SHA512:"), sg.Text("--", key='SHA512')],
        [sg.Text("SHA1:"), sg.Text("--", key='SHA1')],
        [sg.Text("SHA3_512:"), sg.Text("--", key='SHA3_512')],
        [sg.Text("MD5:"), sg.Text("--", key='MD5')]
    ]
    window = sg.Window(title="Hashing", layout=layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == "HASH":
            window['SHA256'].update(Hash.Hash(values["TEXT"], "SHA256"))
            window['SHA512'].update(Hash.Hash(values["TEXT"], "SHA512"))
            window['SHA1'].update(Hash.Hash(values["TEXT"], "SHA1"))
            window['SHA3_512'].update(Hash.Hash(values["TEXT"], "SHA3_512"))
            window['MD5'].update(Hash.Hash(values["TEXT"], "MD5"))
        
if __name__ == "__main__":
    main()