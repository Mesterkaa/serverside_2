import sys
sys.path.append("C:\Repos\Cryptography\Caesar_Code")
import PySimpleGUI as sg
import CaesarCode as Caesar
from importlib import reload # reload 
reload(Caesar)

def main():
    layout = [
        [sg.Button("ENCODE")],
        [sg.Text("Password: (0 to {})".format(len(Caesar.ALPHABET) - 1), size=(15, 1)), sg.Input(key='PASS',size=(2,1),default_text='1')],
        [sg.Button("DECODE")],
        [sg.Multiline(size=(60, 5), key='TEXT', default_text='hello world')]
    ]
    window = sg.Window(title="Caesar-Code", layout=layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == "ENCODE":
            ## Når man trykker ENCODE bruges Caesar-Code funktionen til at kryptere teksten. PASS bliver brugt som kodeord. Tekst feltet bliver så updateret med den krypterede tekst.
            window['TEXT'].update(Caesar.TranslateText(values['TEXT'], Caesar.CreateTranslator(int(values['PASS']))))
        elif event == "DECODE":
            ## Når man trykker DECODE bruges Caesar-Code funktionen til at dekryptere teksten. PASS bliver brugt som kodeord. Tekst feltet bliver så updateret med den dekrypterede tekst..
            window['TEXT'].update(Caesar.TranslateText(values['TEXT'], Caesar.CreateTranslator(int(values['PASS']), True)))
        
if __name__ == "__main__":
    main()