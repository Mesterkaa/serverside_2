import string
ALPHABET = list(string.ascii_lowercase)
ALPHABET.append("æ")
ALPHABET.append("ø")
ALPHABET.append("å")

def CreateTranslator(password: int, revers: bool = False):
    translator = {}
    for index, letter in enumerate(ALPHABET): 
        i = index + password
        if (i >= len(ALPHABET)):
            i = i - (len(ALPHABET))
        if (revers == False):
            translator[ALPHABET[i]] = letter
        else:
            translator[letter] = ALPHABET[i]
    return translator

def TranslateText(text: string, coder: dict):
    newText = ""
    for letter in text:
        if (letter.lower() in ALPHABET):
            newText += coder[letter.lower()]
        else:
            newText += letter
    return newText

