import PySimpleGUI as sg
from enum import Enum
class Fields(Enum):
    Asymmetric = "Asymmetric"
    Caesar_Code = "Caesar_Code"
    Ombyt = "Ombyt"
    Hashing = "Hashing"
    SaltHashing = "Salt"
    XOR = "XOR"

def main():
    layout = [
        [sg.Button(Fields.Asymmetric.value)],
        [sg.Button(Fields.Caesar_Code.value)],
        [sg.Button(Fields.Ombyt.value)],
        [sg.Button(Fields.Hashing.value)],
        [sg.Button(Fields.SaltHashing.value)],
        [sg.Button(Fields.XOR.value)],
        
    ]
    window = sg.Window(title="OmbytBogstavKode", layout=layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == Fields.Asymmetric.value:
            import Asymmetric.gui as gui
            gui.main()
        elif event == Fields.Caesar_Code.value:
            import Caesar_Code.gui as gui
            gui.main()
        elif event == Fields.Ombyt.value:
            import Ombyt_Bogstav_kode.gui as gui
            gui.main()
        elif event == Fields.Hashing.value:
            import Python_Hashing.gui as gui
            gui.main()
        elif event == Fields.SaltHashing.value:
            import Python_Hashing_Salt.gui as gui
            gui.main()
        elif event == Fields.XOR.value:
            import XOR.gui as gui
            gui.main()

        
if __name__ == "__main__":
    main()